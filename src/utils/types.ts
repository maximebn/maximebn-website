type SEO = {
    readonly title: string;
    readonly description: string;
    readonly ogImageUrl: string;
};

type SiteMetaData = {
    readonly title: string | null;
    readonly description: string | null;
    readonly siteUrl: string | null;
};

type Image = {
    readonly node: {
        readonly secure_url: string | null;
        readonly asset_id: string | null;
        readonly context: {
            readonly custom: {
                readonly alt: string | null;
            } | null;
        } | null;
    };
};
