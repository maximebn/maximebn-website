import * as React from 'react';
import BlogListBanner from '../../components/blog/blog-list-banner.component';
import BlogList from '../../components/blog/blog-list.component';
import Layout from '../../components/layout/layout.component';
import { SEO } from '../../components/seo.component';

/**
 *
 * @returns
 */
const BlogListPage = () => {
    return (
        <Layout>
            <section>
                <div className="grid grid-cols-1 md:grid-cols-2 md:pb-0 md:pt-20">
                    <BlogListBanner></BlogListBanner>
                    <BlogList></BlogList>
                </div>
            </section>
        </Layout>
    );
};
export default BlogListPage;

/**
 *
 * @returns
 */
export const Head = () => (
    <SEO
        title="Maxime Boulanghien"
        description="Articles et récits de voyage en Asie Centrale"
        ogImageUrl="https://res.cloudinary.com/dqqj8kpiq/image/upload/v1672080487/maximebn/automne-pamir/0_1_3G0IocV_cvfsjb.jpg"
    ></SEO>
);
