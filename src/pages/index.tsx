import * as React from 'react';
import Layout from '../components/layout/layout.component';
import { SEO } from '../components/seo.component';
import { graphql, useStaticQuery } from 'gatsby';

const IndexPage = () => {
    const data: Queries.LandingGalleryQuery = useStaticQuery(graphql`
        query LandingGallery {
            images: allCloudinaryMedia(filter: {id: {eq: "910cefc4-5094-5f9d-b83c-8925979048a8"}}) {
                edges {
                    node {
                        secure_url
                        context {
                            custom {
                                alt
                            }
                        }
                    }
                }
            }
        }
    `);

    // Landing data
    const edge = data.images.edges[0];

    return (
        <Layout>
            <div className="container">
                <div>
                    <div>
                        <img
                            className="w-full bg-no-repeat object-cover hover:brightness-75 hover:cursor-pointer duration-1000"
                            src={edge.node.secure_url}
                            alt="main photo"
                        />{' '}
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default IndexPage;

/**
 *
 * @returns
 */
export const Head = () => (
    <SEO
        title="Maxime Boulanghien"
        description="Photographies et voyages | Des Pyrénées-Orientales à l'Asie Centrale"
        ogImageUrl="https://res.cloudinary.com/dqqj8kpiq/image/upload/v1669150065/maximebn/vent-kirghize/hEERZ2kMANa22oBxo2ynqG4o.jpg"
    ></SEO>
);
