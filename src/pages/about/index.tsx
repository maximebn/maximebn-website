import * as React from 'react';
import AboutBanner from '../../components/about/about-banner.component';
import AboutGallery from '../../components/about/about-gallery.component';
import Layout from '../../components/layout/layout.component';
import { SEO } from '../../components/seo.component';

/**
 *
 * @returns
 */
const AboutPage = () => {
    return (
        <Layout>
            <section>
                <div className="columns-1 md:columns-2 md:pt-20">
                    <AboutGallery></AboutGallery>
                    <AboutBanner></AboutBanner>
                </div>
            </section>
        </Layout>
    );
};

/**
 *
 * @returns
 */
export const Head = () => (
    <SEO
        title="Maxime Boulanghien"
        description="Parcours"
        ogImageUrl="https://res.cloudinary.com/dqqj8kpiq/image/upload/v1672249338/maximebn/about/about_03_opbcgp.jpg"
    ></SEO>
);

export default AboutPage;
