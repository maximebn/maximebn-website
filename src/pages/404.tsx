import * as React from 'react';
import { Link } from 'gatsby';
import Layout from '../components/layout/layout.component';
import { BsFillChatFill, BsCameraFill } from 'react-icons/bs';

const NotFoundPage = () => {
    return (
        <Layout>
            <div className="container bg-white mt-10 md:mt-40 mx-4 md:mx-0">
                <main className="sm:flex place-content-center">
                    <p className="text-4xl font-extrabold text-sky-600 self-auto sm:text-5xl">404</p>
                    <div className="sm:ml-6 self-auto">
                        <div className="sm:border-l sm:border-gray-200 sm:pl-6">
                            <h1 className="text-4xl font-extrabold text-gray-900 tracking-tight sm:text-5xl">
                                raisons d'aller voir ailleurs ? Non.
                            </h1>
                            <p className="mt-1 text-base text-gray-500">
                                Car c'est sûrement de votre faute mais ça peut arriver. Ne paniquez pas, et consultez
                                directement mes stories, c'est plus simple :
                            </p>
                        </div>
                        <div className="mt-10 columns-1 md:columns-2">
                            <button
                                type="button"
                                aria-label="stories"
                                className="text-white bg-sky-800 hover:bg-sky-900 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center mx-2 my-2"
                            >
                                <BsCameraFill color="white" className="fill-white text-white mr-2 mb-1"></BsCameraFill>
                                <Link to="/">Voir mes stories</Link>
                            </button>
                            <button
                                type="button"
                                aria-label="contact"
                                className="text-white bg-sky-800 hover:bg-sky-900 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center mx-2 my-2"
                            >
                                <Link to="/contact"> Ou contactez-moi mais bon ...</Link>
                                <BsFillChatFill color="white" className="fill-white text-white ml-2 mb-1" />
                            </button>
                        </div>
                    </div>
                </main>
            </div>
        </Layout>
    );
};

export default NotFoundPage;
