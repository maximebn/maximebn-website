import { graphql, PageProps } from 'gatsby';
import * as React from 'react';
import Layout from '../../components/layout/layout.component';
import { SEO } from '../../components/seo.component';

const LegalPage = ({ data }: PageProps<Queries.LegalPageQuery>) => {
    const document = data.allMarkdownRemark.edges[0].node;
    return (
        <Layout>
            <div
                className="text-justify mx-auto max-w-screen-lg prose prose-zinc px-5 md:px-0 md:pt-20"
                dangerouslySetInnerHTML={{ __html: document.html! }}
            ></div>
        </Layout>
    );
};

export const query = graphql`
    query LegalPage {
        allMarkdownRemark(
            filter: { fileAbsolutePath: { regex: "/(legal)/" }, frontmatter: { slug: { eq: "legal" } } }
        ) {
            edges {
                node {
                    id
                    frontmatter {
                        displayTitle
                        title
                        date
                    }
                    html
                }
            }
        }
    }
`;

export default LegalPage;

export const Head = () => <SEO title="Mentions légales" description="Mentions légales" ogImageUrl=""></SEO>;
