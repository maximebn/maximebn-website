import { graphql, PageProps } from 'gatsby';
import * as React from 'react';
import CarbonImpact from '../../components/carbon-impact.component';
import Layout from '../../components/layout/layout.component';
import { SEO } from '../../components/seo.component';

/**
 *
 * @param param0
 * @returns
 */
const CarbonPage = ({ data }: PageProps<Queries.CarbonPageQuery>) => {
    const document = data.allMarkdownRemark.edges[0].node;

    return (
        <Layout>
            <div className="text-justify mx-auto max-w-screen-lg prose prose-zinc px-5 md:px-0 md:pt-20">
                <h1>{document.frontmatter?.displayTitle}</h1>
                <CarbonImpact></CarbonImpact>
                <div className="md:text-justify" dangerouslySetInnerHTML={{ __html: document.html! }}></div>
            </div>
            <div></div>
        </Layout>
    );
};

/**
 *
 */
export const query = graphql`
    query CarbonPage {
        allMarkdownRemark(
            filter: { fileAbsolutePath: { regex: "/(legal)/" }, frontmatter: { slug: { eq: "carbon" } } }
        ) {
            edges {
                node {
                    id
                    frontmatter {
                        displayTitle
                        title
                        date
                    }
                    html
                }
            }
        }
    }
`;

export default CarbonPage;

/**
 *
 * @returns
 */
export const Head = () => (
    <SEO
        title="Impact carbone"
        description="Quel est l'impact carbone d'un site web et comment l'évaluer ?"
        ogImageUrl=""
    ></SEO>
);
