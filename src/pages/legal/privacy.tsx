import { graphql, PageProps } from 'gatsby';
import * as React from 'react';
import Layout from '../../components/layout/layout.component';
import { SEO } from '../../components/seo.component';

/**
 *
 * @param param0
 * @returns
 */
const PrivacyPage = ({ data }: PageProps<Queries.PrivacyPageQuery>) => {
    const document = data.allMarkdownRemark.edges[0].node;
    return (
        <Layout>
            <div
                className="text-justify mx-auto max-w-screen-lg prose prose-zinc px-5 md:px-0 md:pt-20"
                dangerouslySetInnerHTML={{ __html: document.html! }}
            ></div>
        </Layout>
    );
};

/**
 *
 */
export const query = graphql`
    query PrivacyPage {
        allMarkdownRemark(
            filter: { fileAbsolutePath: { regex: "/(legal)/" }, frontmatter: { slug: { eq: "privacy" } } }
        ) {
            edges {
                node {
                    id
                    frontmatter {
                        displayTitle
                        title
                        date
                    }
                    html
                }
            }
        }
    }
`;
export default PrivacyPage;

/**
 *
 * @returns
 */
export const Head = () => (
    <SEO title="Protection des données" description="Politique de protection des données - RGPD" ogImageUrl=""></SEO>
);
