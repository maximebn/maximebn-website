import * as React from 'react';
import ContactBanner from '../../components/contact/contact-banner.component';
import ContactForm from '../../components/contact/contact-form.component';
import Layout from '../../components/layout/layout.component';
import { SEO } from '../../components/seo.component';

/**
 *
 * @returns
 */
const ContactPage = () => {
    return (
        <Layout>
            <section>
                <div className="grid grid-cols-1 md:grid-cols-2 md:pt-20">
                    <ContactBanner></ContactBanner>
                    <ContactForm></ContactForm>
                </div>
            </section>
        </Layout>
    );
};
export default ContactPage;

/**
 *
 * @returns
 */
export const Head = () => <SEO title="Maxime Boulanghien" description="Me contacter | formulaire" ogImageUrl=""></SEO>;
