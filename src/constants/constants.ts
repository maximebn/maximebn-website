export abstract class Constants {
    static readonly STORIES_PATH: string = '/stories/';
    static readonly BLOG_PATH: string = '/blog/';
}
