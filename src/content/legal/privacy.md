---
title: 'Politique de confidentialité'
displayTitle: 'Confidentialité des données'
date: 05/11/2022
slug: 'privacy'
---

# Politique de **confidentialité**

## Propriétaire et responsable du traitement

Maxime Boulanghien

Contact via le formulaire prévu à cet effet : <a href="/contact">ici</a>

## Type de données collectées

Figurent parmi les types de données personnelles que cette application collecte directement ou en recourant à des tiers : **données d'utilisation, nom/prénom et adresse électronique.**

Les données personnelles sont librement fournies par l’utilisateur, ou, en cas de données d’utilisation, collectées automatiquement lorsque vous utilisez ce site. Les utilisateurs qui auraient des doutes sur le type de données personnelles à caractère obligatoires (champs du formulaire de contact) sont invités à me contacter directement par le biais du formulaire de contact ou bien à ne pas me contacter du tout. Toute utilisation des cookies – ou d’autres outils de suivi – par cette application ou par les propriétaires de services tiers utilisés par cette application **vise à fournir un service adéquat à l'utilisateur**. Les utilisateurs sont évidemment responsables de toute donnée personnelle tierce obtenue, publiée ou communiquée par l’intermédiaire de cette application et confirment qu’ils obtiennent le consentement dudit tiers (c'est votre faute, pas la mienne).

## Traitement des données collectées

Outre le propriétaire (moi), les données peuvent être accessibles, dans certains cas, à des parties externes (telles que les fournisseurs tiers de services techniques, les services de messagerie ou les fournisseurs d’hébergement) désignées, le cas échéant, comme sous-traitantes par le propriétaire.

**Formulaire de contact** : En remplissant le formulaire de contact avec ses données, est autorisée l'utilisation de ces renseignements afin de répondre aux demandes d'information tel que l'indique l'en-tête du formulaire. Données collectées : nom et adresse électronique (personnelles) et éventuellement d'autres types de données (méta-données).

**Infrastructure** : ce type de service a pour objet d'héberger des données et fichiers qui permettent à cette application de fonctionner, d’être distribuée et de fournir une infrastructure prête à l’emploi. Ces services fonctionnent à travers des serveurs répartis en Europe (Francfort, Allemagne), bien que la position réelle du stockage des données soit difficile à déterminer. Pour information, le service de gestion du développement utilisé est GitLab. Le service d'hébergement est Netlify (voir [Mentions légales](/legal)). Aucune donnée à caractère personnel et identifiable n'est stockée sur l'un ou l'autre des deux services précédemment mentionnés. Ouf !

**Google ReCaptcha** : l'éviter aurait été mieux mais les petits malins sont nombreux ! Du coup, est utilisé Google reCAPTCHA, service fourni par Google pour vérifier si les données saisies sur le formulaire de contact l'ont été par un utilisateur ou par un programme automatisé. Pour ce faire, reCAPTCHA analyse le comportement du visiteur du site en fonction de différentes caractéristiques (par exemple, l’adresse IP, la durée pendant laquelle le visiteur a été sur le site Web ou les mouvements de souris effectués par l’utilisateur). Cette analyse démarre automatiquement dès que le visiteur du site web entre sur le site. Cela va sans dire, ces données sont collectées et transmises à Google. Et tout ceci se déroule en arrière-plan : vous n'êtes jamais informés qu’une telle analyse est en cours. C'est intrusif, je l'accorde, mais c'est un moyen de protection relativement efficace.

Plus d’informations sur Google reCAPTCHA :

[Politique de confidentialité Google](https://policies.google.com/privacy?hl=fr) | [Le service ReCaptcha de Google](https://www.google.com/recaptcha/intro/)

## Utilisation des données collectées

Rassurez-vous, **les données collectées ne servent qu'à vous proposer le service minimum** . Entre autres, elles permettent surtout de vous répondre directement par le biais de votre adresse mail lorsque vous me contactez au moyen de la page formulaire.

Aucune analyse statistique de fréquentation du site n'est mise en place (type Google Analytics), vu le peu d'intérêt que cela présente. Je vous invite tout de même à surveiller d'éventuelles mises à jour de cette politique de confidentialité, dans la mesure où la mise en place de tels outils serait invisible pour l'utilisateur final.

Je suis **le seul propriétaire du peu d'informations recueillies** sur ce site. Je ne vends, ni n'échange, ni ne transfère aucune de vos informations personnelles identifiables (pour rappel : nom et adresse électronique) à aucun tiers (heureusement parce que vous ne vous battez pas pour me contacter). Encore une fois, cela ne comprend pas d'éventuelles tierces parties qui permettent de faire tourner ce site. Se référer à la partie Traitement des données collectées pour plus d'informations à ce sujet.

## Temps de conservation des données collectées

Les données collectées sont conservées aussi longtemps que requis pour la finalité pour laquelle elles ont été collectées. Ce qui veut dire en d'autres termes, pas bien longtemps si votre message n'est pas intéressant, et un peu plus longtemps si nous sommes amenés à échanger.

## Logs et maintenance

A des fins de maintenance, cette application et/ou des services tiers sont susceptibles de collecter des données de monitoring, c'est-à-dire décrivant différentes interactions avec l'application (System Logs), et/ou d'utiliser d'autre type de données (adresse IP, date ..).

## Droits des utilisateurs

Vous pouvez exercer certains droits sur les données ici traitées. En particulier, vous avez la possibilité de :

-   **Retirer votre consentement** à tout moment
-   Vous **opposer au traitement** des données (ce qui serait quand même bête, car reviendrait à me contacter pour que je ne puisse pas vous répondre)
-   **Accéder à vos données** (nom/prénom, adresse mail, archives)
-   Faire **supprimer ou effacer vos données** collectées ; c'est-à-dire nom/prénom, adresse mail et messages archivés, ce qui a de grandes chances d'être déjà le cas au moment où vous le demanderez, à moins que vous n'en fassiez la demande immédiatement après m'avoir contacté, ce qui là n'aurait pas de sens, sinon de m'embêter 🗑️.
