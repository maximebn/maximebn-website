---
title: 'Mentions légales'
displayTitle: 'Mentions légales'
date: 06/11/2022
slug: 'legal'
---

# Mentions légales

## Editeur du site

Ce site a été conçu, développé, déployé et est édité par moi-même, Maxime Boulanghien, j'en suis le seul administrateur.

## Hébergement

Ce site est déployé grâce aux services d'hébergement de Netlify, Inc., situé au 44 Montgomery Street, Suite 300, San Francisco, California 94104. Physiquement, les serveurs sur lesquels sont déployés ce site se situent dans l'Union Européenne (Francfort, Allemagne).

## Conditions d'utilisation

L’utilisation et la consultation de ce site web implique l’acceptation pleine et entière des conditions générales d’utilisation décrites ici, notamment en matière de propriété intellectuelle. Ces conditions d’utilisation sont susceptibles d’être modifiées ou complétées à tout moment.

Ce site est accessible à tout moment aux visiteurs. Bien que les mises à jour régulières du site n'entraînent normalement pas d'interruption de service, la disponibilité continue de ce site ne peut pas être garantie, et pour des raisons techniques ou fonctionnelles, une interruption peut être décidée et mise en oeuvre à tout moment.

Les informations fournies sur ce site ne reflètent qu'une vision subjective d'un auteur. Je ne peux donc être tenu pour responsable des oublis, inexactitudes ou carences de ces informations, notamment au regard de leur évolution dans le temps.

## Propriété intellectuelle

Je suis le seul propriétaire des droits de propriété intellectuelle de toutes les photos que vous verrez sur ce site. Il en est de même pour les différents textes qui les accompagnent. Sont donc interdites toutes reproductions, publications, modifications ou adaptations de tous les éléments de ce site, quel que soit le moyen utilisé. Toute exploitation non autorisée est évidemment considérée comme contrefaçon. N'hésitez pas cependant à vous rapprocher de moi si ce cas de figure se présente, je ne mords pas très fort.

Les illustrations, en revanche, sont libres de droit : elles peuvent librement être téléchargées et utilisées dans n'importe quel projet personnel ou commercial, conformément aux conditions d'utilisations de [unDraw](https://undraw.co/license). En revanche, et évidemment, <strong>aucune redistribution</strong> n'est autorisée. Bien qu'aucune attribution ne soit nécessaire, c'est peu de mentionner que [unDraw](https://undraw.co) est un superbe projet open-source régulièrement maintenu et mis à jour, déjà [largement adopté](https://undraw.co/thankful) !

## Liens hypertexte

Ce site contient un certain nombre de liens hypertextes vers d’autres sites externes, qui, au moment de leur publication, ont été choisi pour :

-   leur pertinence en rapport avec les propos qu'ils illustrent,
-   le complément d'information apporté à un propos,
-   mettre à disposition du lecteur la source d'un propos.

Cependant, je n'ai ni la possibilité ni les moyens de vérifier l'évolution du contenu de ces sources d'informations externes, au regard notamment des mises à jour ultérieures à leur premier référencement sur ce site pouvant affecter l'un des trois objets mentionnés ci-dessus. De ce fait, je n'assume absolument aucune responsabilité quant aux informations consultées sur ces sites externes.

## Cookies

Les cookies, s'il y en a, susceptibles d'être installés sur votre navigateur web ne visent qu'à vous fournir un service minimum. Aucun traceur lié à des opérations publicitaires, mesures d'audience ou partage par réseaux sociaux n'est ici utilisé. Les seuls cookies susceptibles d'être installés sont liés à la persistance de choix d'interface, plus particulièrement de choix de langue (i18n, non implémentée à l'heure actuelle), et ne nécessitent pas d'obtenir votre consentement préalable. N'oubliez pas de configurer correctement votre navigateur web pour vous aider à exercer valablement votre choix face aux cookies. De nombreuses ressources sont disponibles pour vous aider ; la CNIL propose par exemple une page d'information concise à ce sujet :

[Sites web, cookies et autres traceurs.](https://www.cnil.fr/fr/site-web-cookies-et-autres-traceurs)
