---
title: "Evaluation de l'empreinte carbone"
displayTitle: "Evaluation de l'empreinte carbone"
date: 18/11/2022
slug: 'carbon'
---

## Quel est l'impact de ce site web ?

L'empreinte carbone de ce site web a été mesurée par <a href="https://www.websitecarbon.com/">Website Carbon Calculator</a>. Chaque page chargée émet entre 0.07g et 0,19g d'<a href="https://ec.europa.eu/eurostat/statistics-explained/index.php?title=Glossary:Carbon_dioxide_equivalent/fr#:~:text=L'%C3%A9quivalent%20dioxyde%20de%20carbone,de%20carbone%20ayant%20le%20m%C3%AAme">équivalent CO2</a>, ce qui place ce site :

-   dans les 20% des sites web audités les plus sobres pour les pages les moins performantes (celles contenant une galerie de photos),
-   dans les 7% des sites web audités les plus sobres pour les pages les plus propres 🦤.

Son code source peut librement être consulté sur <a href="https://gitlab.com/maximebn">Gitlab</a>.

Le <a href="https://sustainablewebdesign.org/calculating-digital-emissions/">calcul de cette empreinte</a> n'est évidemment pas simple, et s'il est important d'évaluer la moyenne des émissions CO2 par page chargée, on admettra que le sujet deviendra d'autant plus important que le trafic associé au site audité l'est. Je ne connais absolument pas l'affluence associée à mon site web, mais il y a fort à parier que le kilo de CO2 est assez loin, et que comparativement à l'<a href="https://www.carbone4.com/myco2-empreinte-moyenne-evolution-methodo">émission moyenne d'un français de 9,9 tCO2eq par personne en 2022</a>, c'est un non-sujet.

## Et le secteur du numérique ?

On sort un peu du périmètre mais tant qu'à faire. L'impact du secteur numérique sur l'environnement est sujet d'attention croissant. Selon <a href="https://theshiftproject.org/article/pour-une-sobriete-numerique-rapport-shift/">The Shift Project</a>, le numérique représentait en 2019 3,5% des émissions de gaz à effet de serre (GES) mondiales. Si cette part demeure modeste comparativement à d’autres secteurs, sa croissance constante (+6%/an) doit nous interroger, d'autant plus qu'il ne s'agit là que des émissions de gaz à effet de serre, et non de la totalité des externalités négatives dont le secteur est responsable.

Il existe évidemment des technologies obsolètes et/ou délétères, dans le secteur numérique ou d'autres, dont les impacts environnementaux et sociétaux peuvent globalement être considérés comme négatifs. D'autres technologies ne méritent pas tant que l'on remette en cause leur utilisation sinon leur usage. Dans bien des cas, l'usage du numérique peut constituer une alternative de moindre mal et même participer à la réduction des émissions de gaz à effets de serre : assister à une visio-conférence plutôt que de se déplacer pour y assister par exemple. Mais même cet usage est responsable d'externalités environnementales négatives : gaz à effet de serre, consommation d'énergie primaire, utilisation de ressources abiotiques et d'eau douce ou augmentation du volume de déchets non recyclés. Comment, alors, différencier le bon usage du mauvais usage ? Après tout, il y a bien le bon chasseur et le mauvais chasseur 🔫. Une <a href="https://theshiftproject.org/article/climat-insoutenable-usage-video/#:~:text=(Mise%20%C3%A0%20jour%20de%20Juin,qui%20ne%20sont%20pas%20contest%C3%A9s.">étude de the Shift Project</a> publiée en 2019 montre par exemple que la vidéo en ligne représente 60% des flux de données mondiaux, ce qui est conséquent ! Les usages qui en sont faits sont-ils toujours justifiés et justifiables ? Il suffira de consulter le lien vers l'étude pour se convaincre du contraire, et que la question du progrès peut alors être posée ...

> Nous vivons dans un monde où une seule forme d’usage du numérique, la vidéo en ligne, génère 60 % des flux de données mondiaux et plus de 300 millions de tonnes de CO2 par an.

D’une manière générale, la notion de progrès doit être questionnée par rapport à la question environnementale, la question sociale et sociétale et ne doit pas se résumer au seul progrès technique. Il faut en fait l'aborder de façon holistique, il s'agit même de l'un <a href="https://unesdoc.unesco.org/ark:/48223/pf0000117740_fre">des sept savoirs nécessaires à l'éducation du futur</a> selon Edgar Morin :

> En effet, il y a une inadéquation de plus en plus ample, profonde et grave entre, d'une part, nos savoirs disjoints, morcelés, compartimentés et, d'autre part, des réalités ou problèmes de plus en plus polydisciplinaires, transversaux, multidimensionnels, transnationaux, globaux, planétaires.

<a href="https://ecoinfo.cnrs.fr/2019/04/30/introduction-aux-impacts-environnementaux-du-numerique/">Quelques priorités</a> peuvent se dégager :

-   Améliorer notre compréhension des systèmes sociaux et environnementaux, et leurs interactions,
-   Appliquer le principe de précaution aux technologies du numérique : là où il est appliqué à de nombreux autres domaines, le numérique y échappe. Comme tout autre secteur, les conséquences sociales, sociétales, environnementales et sanitaires d'une technologie devraient être évaluées avant que celle-ci soit ne soit largement déployée et adoptée,
-   Questionner nos usages des technologies numériques,
-   Penser les alternatives technologiques sobres, selon une approche systémique technique, sociale, environnementale, et donc trans-disciplinaire.

Preuve que le sujet n'est pas simple : les débats sont nombreux dans le métier et il s'agit d'un sujet largement abordé aujourd'hui. Pour autant, ils ne se concentrent essentiellement que sur le dernier point, à savoir l'émergence de technologies plus sobres et efficaces, souvent sans aucune approche systémique. Par exemple, l'AFNOR a récemment publié un <a href="https://www.boutique.afnor.org/fr-fr/norme/afnor-spec-2201/ecoconception-des-services-numeriques/fa203506/323315">guide de référence pour éco-concevoir nos services numériques</a>. S'il est évidemment de la plus grande utilité, et devrait être lu par tous les professionnels du secteur, ces initiatives ne représentent qu'une contribution très modeste à la réflexion globale que nous devons engager. En juin 2020, <a href="https://www.senat.fr/fileadmin/Fichiers/Images/redaction_multimedia/2020/2020-Documents_pdf/20200624_Conf_presse_Dev_Dur/20200624_Conf_Dev_Dur_Infographie.pdf">25 propositions pour une transition numérique écologique</a> ont été publiées par une mission d'information du Sénat (Commission de l'améngament du territoire et du développement durable), parmi lesquelles l'usage des applications numériques, des terminaux, des vidéos est questionné. L'on y parle même de régulation de l'attention, ce qui pourrait nous renvoyer aux inquiétudes de Gérald Bronner liées à la dérégulation du <em>marché cognitif</em>, détaillées dans son livre <a href="https://www.radiofrance.fr/franceculture/podcasts/la-grande-table-idees/surinformation-trop-d-ecrans-attention-deraison-7460009">Apocalypse Cognitive</a> paru en 2020 ...
