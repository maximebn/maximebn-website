---
title: 'Retour au Royaume'
description: 'Un Cambodge musulman, entre désir global et expressions locales'
shortDescription: 'Un Cambodge musulman ...'
place: 'Cambodge'
year: '2017'
date: '2022-11-06'
mapLink: 'https://www.openstreetmap.org/export/embed.html?bbox=103.76998901367189%2C11.805522732159929%2C106.06338500976564%2C13.0206140544476&amp;layer=cyclemap'
openStreetMapLink: 'https://osm.org/go/4YnGn8?layers=C'
icon: '/icons/mosque.svg'
titleColor: 'amber-500'
slug: 'retour-au-royaume'
cloudinaryFolder: 'maximebn/retour-au-royaume'
---

Du Royaume de Champa, que reste-t-il ? La lente et lourde diffusion des vibrations sonores d’un tambour appelant à la prière, ou le départ en fanfares des bateaux de pêche des villes portuaires, autrefois lieux de commerce et de piraterie symbolisant la prospérité des échanges avec les îles indonésiennes ? La cacophonie ambiante de la fête du Katê, moment d’union avec les dieux, ou le calme d’un village Cham musulman pendant une journée du ramadân ?
<br/>

Peuple d’origine austronésienne, et originellement hindouiste, une partie des Chams est devenue musulmane lors de l’expansion sud-asiatique de l’Islam à partir du XVIème siècle, notamment au contact des nombreux marchands arabes, indiens ou persans. Cette introduction tardive de l’Islam dans la région pourrait, éventuellement, expliquer la persistance d’un socle hindouiste dans la pratique de l’Islam, considéré comme hétérodoxe par le monde musulman.
<br/>
<br/>

Cette série se concentre sur cette minorité musulmane que constitue la communauté Cham au Cambodge. Aujourd’hui à majorité sunnite, elle renferme en son intérieur une autre minorité : les Chams de l’Imam San, du nom du saint fondateur de leur communauté. Ils pratiquent un Islam véridique mais singulier, aux diverses manifestations cultuelles joyeuses, chaleureuses et éclatantes, largement rapportées par divers travaux scientifiques. Mais par Cham, on entend aussi parfois désigner les Chams eux-même, les malais ou les khmers musulmans sunnites, autrement dit, tous les musulmans cambodgiens se définissant tantôt comme Khmer Islam, tantôt comme Cham Jvea. Entre l’élasticité des exonymes et la rigidité des endonymes de chaque communauté, les Chams constituent indéniablement une fresque multicolore à l’identité culturelle composite insaisissable, perdue ou peut-être simplement méconnue.
