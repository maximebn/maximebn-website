---
title: 'Un automne au Pamir'
description: "Coeur de l'ismaélisme nizârite, entre Tian Shan, Hindu Kush et Karakorum."
shortDescription: "Coeur de l'ismaélisme nizârite ..."
place: 'Tadjikistan'
year: '2017'
date: '2022-11-06'
mapLink: 'https://www.openstreetmap.org/export/embed.html?bbox=71.63085937500001%2C36.97622678464096%2C73.92425537109376%2C37.96368875328561&amp;layer=cyclemap'
openStreetMapLink: 'https://osm.org/go/zxuwRP?layers=C'
icon: '/icons/praying-hands.svg'
titleColor: 'emerald-700'
slug: 'automne-au-pamir'
cloudinaryFolder: 'maximebn/automne-pamir'
---

Sur les rives du Panj, les effluves du blé fraîchement battu par les Wakhis se mêlaient aux poussières soulevées par le vent. Au loin, les cimes enneigées de Marx et Engels grelottaient d’impatience que l’hiver n’arrive alors que l’automne venait tout juste de faire son apparition. Au bord du Bartang, les derniers mariages faisaient danser les tadjiques pleines de grâce, quelques somonis à la main, dans l’insouciance du bonheur que procure l’union symbolique de deux villages. Au rythme des dombras, l’automne paraissait même s’éloigner pour laisser place à la gaieté d’un été. Les méandres du Chok Dara, eux, transforment le vert en poussière, l’abondance en carences mais sans jamais remplacer l’hospitalité de ses habitants par l’hostilité des lieux. Tant que l’eau est là, rien ne manquerait.
<br/>
<br/>

L’automne au Pamir, dans le haut Badakhchân, c’est tout ça. Cœur de l’Ismaélisme nizârite, le Pamir tadjique fût le refuge de Nasir Khusraw, un éminent dâ'i de l'époque d'al-Mustansir, à partir de 1060. Ayant joué un rôle crucial dans la propagation de l'Ismaélisme dans les régions orientales reculées du monde iranien, il est considéré comme fondateur des communautés du Hindu Kush et du Badakhshân. Plus tard, suite aux invasions mongole, de nombreux nizârites persans rejoignirent la région qui avait échappé à la débâcle mongole, et c'est ainsi que fût reconnu l'imamat nizârite durant la chute d'Alamut alors que les anciennes religions iraniennes étaient encore largement pratiquées. Malgré des siècles de discrétion, les dignes héritiers du Vieux de la Montagne ne sauraient cacher, sous leurs sourires et leur bonté, la rudesse d’une vie dans un simple habit.
