---
title: 'Kelin Salam'
description: 'Bienvenue à la mariée'
shortDescription: 'Bienvenue à la mariée'
place: 'Kirghizistan'
year: '2017'
date: '2022-11-06'
mapLink: 'https://www.openstreetmap.org/export/embed.html?bbox=72.49053955078126%2C41.212754601947125%2C73.63723754882814%2C41.679066225164114&amp;layer=cyclemap'
openStreetMapLink: 'https://osm.org/go/z07~JKw-?layers=C'
icon: '/icons/venus-mars.svg'
titleColor: 'amber-800'
slug: 'kelin-salam'
cloudinaryFolder: 'maximebn/kelin-salam'
---

Agenouillée aux côtés de ses aînés, elle tendait ses mains, ouvertes vers le ciel, dans un calme dévot qui rompait l’ardeur festive du moment. Hommes, femmes et enfants commençaient à s’agglutiner autour d’elle en chuchotant. Les effluves de bois cendré et viande grillée alertaient tout de même sur l’absence de quelques bougres restés au loin, à l’écart pour parfaire la cuisson des chachliks. Une des aînées de la famille lui tenait fermement les poignets. Elle, dont on ne pouvait distinguer ni le visage, ni le regard, sous son majestueux voile blanc orné de motifs argentés, était passive. Les membres de la famille s’apprêtaient tour à tour à verser dans ses mains la farine qui avait été disposée au sol, sur un drap blanc soigneusement déplié avant le début de la cérémonie. Ses mains s’emplissaient peu à peu de farine avant que ses proches ne viennent l’en débarrasser en lui serrant la main de manière légataire. Certains gestes prêtaient parfois à confusion. Les voix s’élevaient, provoquant de soudains débats au creux de gesticulations volontiers théâtrales. Puis le calme revenait. Les mêmes gestes étaient répétés avec du beurre fondu, puis avec du plov. Les deux aliments passés par les mains de la mariée, chaque membre de la famille s'en voyait alors octroyée une petite part à avaler. Terre nourricière devenait mère nourricière. La jeune mariée, désormais, nourrira sa nouvelle famille de ses mains, sa belle-famille, chez qui elle allait s’installer.
<br/>
<br/>

De l'été jusqu'à l'automne clément, c'est la saison des mariages au Kirghizistan. Ici, les Ouzbèques font vivre un héritage traditionnel lors d'une cérémonie de mariage de deux jeunes époux pour qui tout changera à compter de ce jour.
