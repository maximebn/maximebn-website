---
title: 'Un vent kirghize'
description: 'Du Pamir au Tian Shan'
shortDescription: 'Du Pamir au Tian Shan'
place: 'Kirghizistan'
year: '2017'
date: '2022-11-06'
mapLink: 'https://www.openstreetmap.org/export/embed.html?bbox=67.29675292968751%2C38.492294192361356%2C76.4703369140625%2C42.28137302193453&amp;layer=cyclemap'
openStreetMapLink: 'https://osm.org/go/z0sX7A-?layers=C'
icon: '/icons/horse.svg'
titleColor: 'cyan-500'
slug: 'vent-kirghize'
cloudinaryFolder: 'maximebn/vent-kirghize'
---

Sur les hauts plateaux du Pamir ou dans les montagnes du Tian Shan, les nomades kirghizes installent leur yourte d’été en altitude, dans une grande tradition pastorale, afin d’y faire paître leurs animaux, chèvres, moutons, vaches, yaks ou chevaux. Particulièrement habiles à cheval et dès le plus jeune âge, leurs talents de cavalier en faisaient autrefois de grands guerriers, tantôt avec, tantôt contre les Mongols. Aujourd’hui, pas vraiment bagarreurs pour un sou, les nomades kirghizes cultivent leur proximité à la nature, la simplicité, le contentement et le partage dans une sagesse solitaire, essayant tant bien que mal de transmettre ce legs aux dernières générations qui n’ont pas échappé à l’attrait du numérique et des villes galopantes.
<br/>
<br/>
Malgré parfois la rudesse de leurs conditions de vie, les nomades aiment les montagnes, leur espace et la liberté retrouvée des lentes secondes inlassablement égrénées par le vent. Sous la casquette délavée d'un berger rustre, sous les traits sombres et taillés par les éléments d'un oncle taciturne, ou ceux qui reflètent les saisons passées d'une grand-mère aux airs sévères, se cachent toujours une viduité et un éloignement qui sacralisent une chaleureuse bienveillance. Les nomades kirghizes, aujourd'hui semi-nomades pour la plupart, jamais n'oublient leur essentiel : être libre. Libre comme le vent, au moins le temps d'une ou deux saisons.
