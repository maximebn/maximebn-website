import * as React from 'react';
import { SiMatrix } from 'react-icons/si';

const ContactBanner = () => {
    return (
        <div className="w-full px-4 md:pl-12">
            <h2 className="text-primary text-lg font-bold mb-2 text-center md:text-start">Une question ?</h2>
            <h2 className="text-sky-800 text-3xl font-extrabold md:text-4xl text-center md:text-start">Un message ?</h2>
            <div className="text-primary text-sm mb-6 md:mb-16 text-center md:text-start mt-2">
                Renseignez ce formulaire 🪁 ou retrouvez-moi sur
                <a href="https://matrix.to/#/@maximebn:matrix.org">{' Matrix '} </a>🔮, je vous répondrai au plus vite !
            </div>
            <img className="w-10/12 hidden md:block md:mb-5" src="/vectors/undraw_faq_re_31cw.svg" alt="contact-me" />
        </div>
    );
};

export default ContactBanner;
