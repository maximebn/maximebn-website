import React, { useRef, useState } from 'react';
import { RiSendPlaneLine } from 'react-icons/ri';
import ReCAPTCHA from 'react-google-recaptcha';
import { Link, navigate } from 'gatsby';

const ContactForm = () => {
    const [state, setState] = useState({});
    const recaptchaRef = useRef<ReCAPTCHA>(null);

    /**
     *
     * @param event
     */
    const handleChange = (event: any) => {
        setState({ ...state, [event.target.name]: event.target.value });
    };

    /**
     * Le token reCapatcha doit être récupéré avant de soumettre le formulaire Netlify. Implémentation custom : https://blog.larsbehrenberg.com/how-to-create-a-contact-form-with-recaptcha-hosted-on-netlify
     * @param event
     */
    const handleSubmit = async (event: any) => {
        event.preventDefault();
        const form = event.target;
        const recaptchaValue = await recaptchaRef.current?.executeAsync();

        fetch('/', {
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            body: new URLSearchParams({
                'form-name': form.getAttribute('name') as string,
                'g-recaptcha-response': recaptchaValue as string,
                ...state
            })
        })
            .then(() => navigate(form.getAttribute('action')))
            .catch((error) => alert(error));
    };

    return (
        <div className="block w-full px-4 my-auto md:mt-12">
            <form
                onSubmit={handleSubmit}
                method="post"
                netlify-honeypot="bot-field"
                data-netlify="true"
                data-netlify-recaptcha="true"
                action="/contact/success"
                name="contact"
            >
                <input type="hidden" name="bot-field" />
                <input type="hidden" name="form-name" value="contact" />
                <div>
                    <label className="block mb-2 text-sm text-gray-900">
                        Votre adresse mail
                        <input
                            type="email"
                            name="email"
                            className="shadow-sm border border-gray-100 text-gray-900 text-sm rounded-lg block w-full p-2.5 mt-2 mb-8"
                            placeholder="adresse@mail.com"
                            onChange={handleChange}
                            required
                        ></input>
                    </label>
                </div>
                <div>
                    <label className="block mb-2 text-sm text-gray-900">
                        Sujet du message
                        <input
                            type="text"
                            name="subject"
                            className="block p-3 w-full text-sm text-gray-900 rounded-lg border border-gray-100 shadow-sm mt-2 mb-8"
                            placeholder="Dites-moi pourquoi vous voulez me contacter"
                            onChange={handleChange}
                            required
                        ></input>
                    </label>
                </div>
                <div className="md:col-span-2">
                    <label className="block mb-2 text-sm text-gray-900">
                        Votre message
                        <textarea
                            name="message"
                            rows={4}
                            className="block p-2.5 w-full text-sm text-gray-900 rounded-lg shadow-sm border border-gray-100 mt-2 mb-4"
                            placeholder="Faites court pour préserver notre nouvelle amitié"
                            onChange={handleChange}
                            required
                        ></textarea>
                    </label>
                </div>
                <div className="text-xs text-zinc-700 text-center md:text-end mb-5">
                    <em>
                        En soumettant ce formulaire, vous confirmez avoir lu la{' '}
                        <Link to="/privacy" className="inline text-xs text-blue-700 block">
                            politique de confidentialité des données
                        </Link>
                        .
                    </em>
                </div>
                <div className="mb-4 grid lg:grid-cols-2">
                    <ReCAPTCHA
                        ref={recaptchaRef}
                        sitekey={`${process.env.GATSBY_SITE_RECAPTCHA_KEY}`}
                        size="invisible"
                    ></ReCAPTCHA>
                    <button
                        aria-label="send-message"
                        type="submit"
                        className="text-white bg-sky-900 hover:bg-sky-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center m-auto md:mb-0 md:mr-0 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:bg-sky-700 duration-300"
                    >
                        Envoyer mon message
                        <RiSendPlaneLine color="white" className="fill-white text-white ml-2" />
                    </button>
                </div>

                <div className="text-center md:text-start"></div>
            </form>
        </div>
    );
};

export default ContactForm;
