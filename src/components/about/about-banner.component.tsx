import * as React from 'react';

const AboutBanner = () => {
    return (
        <div className="w-full md:mt-10 px-4 lg:mt-0 py-8">
            <span className="text-primary mb-2 block text-lg font-semibold">En mouvement</span>
            <h2 className="text-sky-800 mb-8 text-3xl font-extrabold sm:text-4xl">
                {' '}
                Entre usage de la liberté et curiosité
            </h2>
            <p className="text-body-color mb-8 text-sm text-justify">
                Mes pieds, mon appareil photo, une mashroutka ou encore mon pouce m'amènent à m'immerger dans des
                univers singuliers, à assister avec abandon à la sublimation des femmes, des hommes, des peuples et de
                leur identité culturelle, une histoire perpétuelle du temps ...
            </p>
            <p className="text-body-color md:mb-12 text-sm text-justify">
                Ingénieur de métier, j'ai souhaité préparé plusieurs séries photographiques - dont vous découvrirez ici
                des extraits - après avoir passé une année en Asie. Sur les traces silencieuses de Champa, aux confins
                de l'Ismaélisme, dans les contreforts d'une montagne silencieuse, et aujourd'hui dans les
                Pyrénées-Orientales, les espaces m'attirent, s'étirent, et m'émerveillent. Aujourd'hui, je cherche une
                approche équilibrée qui m'amènera à concilier une photographie éthique et un mode de déplacement
                responsable.
            </p>
        </div>
    );
};

export default AboutBanner;
