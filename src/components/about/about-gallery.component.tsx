import { graphql, useStaticQuery } from 'gatsby';
import * as React from 'react';

const AboutGallery = () => {
    // Données = le contenu du dossier /stories sous content
    const data: Queries.AboutGalleryQuery = useStaticQuery(graphql`
        query AboutGallery {
            images: allCloudinaryMedia(filter: { folder: { eq: "maximebn/about" } }) {
                edges {
                    node {
                        secure_url
                        context {
                            custom {
                                alt
                            }
                        }
                    }
                }
            }
        }
    `);

    // Gallery data
    const edges = data.images.edges;
    const about01 = edges.find((item) => item.node.secure_url?.includes('about_01'));
    const about02 = edges.find((item) => item.node.secure_url?.includes('about_02'));
    const about03 = edges.find((item) => item.node.secure_url?.includes('about_03'));

    return (
        <div className="w-full px-4 flex items-center">
            <div className="w-0 md:w-5/12 lg:w-4/12">
                <div className="py-3 sm:py-4">
                    <img
                        src={about03?.node.secure_url!}
                        alt={about03?.node.context?.custom?.alt!}
                        className="w-full rounded-2xl"
                    />
                </div>
                <div className="py-3 sm:py-4">
                    <img
                        src={about02?.node.secure_url!}
                        alt={about02?.node.context?.custom?.alt!}
                        className="w-full rounded-2xl"
                    />
                </div>
            </div>
            <div className="w-0 px-3 sm:px-4 md:w-6/12 lg:w-5/12">
                <div className="relative z-10 my-4">
                    <img
                        src={about01?.node.secure_url!}
                        alt={about01?.node.context?.custom?.alt!}
                        className="w-full rounded-2xl"
                    />
                </div>
            </div>
        </div>
    );
};

export default AboutGallery;
