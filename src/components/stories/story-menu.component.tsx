import * as React from 'react';
import { Popover, Transition } from '@headlessui/react';
import { graphql, Link, useStaticQuery } from 'gatsby';
import { Constants } from '../../constants/constants';
import { useLocation } from '@reach/router';

interface Props {
    mobile: boolean;
}

const StoryMenu = ({ mobile }: Props) => {
    // Données = le contenu du dossier /stories sous content
    const data: Queries.StoriesQuery = useStaticQuery(graphql`
        query Stories {
            allMarkdownRemark(filter: { fileAbsolutePath: { regex: "/(stories)/" } }) {
                nodes {
                    frontmatter {
                        title
                        icon
                        shortDescription
                        slug
                    }
                    id
                }
            }
        }
    `);

    // Template data
    const stories = data.allMarkdownRemark.nodes;
    const location = useLocation();
    const label = mobile ? 'stories' : '+ stories';

    return (
        <Popover className="relative">
            {({ open }) => (
                <>
                    <Popover.Button className="group rounded-md inline-flex items-center text-base focus:outline-none focus:ring-offset-2">
                        <span
                            className={
                                location.pathname.includes(Constants.STORIES_PATH) ? 'font-medium text black' : ''
                            }
                        >
                            {label}
                        </span>
                    </Popover.Button>

                    <Transition
                        as={React.Fragment}
                        enter="transition ease-out duration-200"
                        enterFrom="opacity-0 translate-y-1"
                        enterTo="opacity-100 translate-y-0"
                        leave="transition ease-in duration-150"
                        leaveFrom="opacity-100 translate-y-0"
                        leaveTo="opacity-export translate-y-1"
                    >
                        <Popover.Panel className="absolute z-10 left-1/2 transform -translate-x-1/2 mt-3 px-2 w-screen max-w-xs sm:px-0">
                            <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 overflow-hidden">
                                <div className="relative grid gap-6 bg-white px-5 py-6 sm:gap-8 sm:p-8">
                                    {stories.map((item: any) => (
                                        <Link
                                            to={Constants.STORIES_PATH.concat(item.frontmatter.slug)}
                                            key={item.id}
                                            activeClassName="text-zinc-900 font-medium"
                                        >
                                            <div className="-m-3 p-3 flex items-start rounded-lg hover:bg-gray-50 transition ease-in-out duration-150">
                                                <img
                                                    src={item.frontmatter.icon}
                                                    className="w-5 pt-1 content-center"
                                                    alt="icon"
                                                />
                                                <div className="ml-4">
                                                    <div>{item?.icon}</div>

                                                    <p className="text-base text-gray-900">
                                                        {item?.frontmatter?.title}
                                                    </p>
                                                    <p className="mt-1 text-sm text-gray-500">
                                                        {item?.frontmatter?.shortDescription}
                                                    </p>
                                                </div>
                                            </div>
                                        </Link>
                                    ))}
                                </div>
                            </div>
                        </Popover.Panel>
                    </Transition>
                </>
            )}
        </Popover>
    );
};
export default StoryMenu;
