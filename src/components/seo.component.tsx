import React from 'react';
import { useSiteMetadata } from '../hooks/use-site-metadata';

export const SEO = ({ title, description, ogImageUrl }: SEO) => {
    const siteMetaData = useSiteMetadata();

    const seo = {
        title: title || siteMetaData?.title,
        description: description || siteMetaData?.description,
        siteUrl: siteMetaData?.siteUrl,
        ogImageUrl
    };

    return (
        <>
            <title>{seo.title}</title>
            <meta name="description" content={seo.description!} />
            <meta name="og:url" content={seo.siteUrl!} />
            <meta property="og:type" content="website"></meta>
            <meta property="og:title" content={seo.title!}></meta>
            <meta property="og:description" content={seo.description!}></meta>
            <meta property="og:url" content={seo.siteUrl!}></meta>
            <meta property="og:image" content={seo.ogImageUrl}></meta>
        </>
    );
};
