import * as React from 'react';

const CarbonImpact = () => {
    return (
        <div>
            <div className="inline-flex rounded-md shadow-sm">
                <div className="py-2 px-4 text-sm font-medium text-blue-900 bg-white rounded-l-lg border-emerald-300 border-2 border-r-0">
                    0.07g of CO2/view
                </div>
                <a
                    href="https://websitecarbon.com/"
                    target="blank"
                    className="py-2 px-4 text-sm font-black no-underline text-white bg-blue-900 rounded-r-md border border-blue-900 border-2 border-l-0"
                >
                    Website Carbon
                </a>
            </div>
            <div className="text-sm font-medium mt-1 text-blue-900 ml-8">Cleaner than 93% of pages tested</div>
        </div>
    );
};

export default CarbonImpact;
