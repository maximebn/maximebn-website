import * as React from 'react';
import { Link } from 'gatsby';
import StoryMenuComponent from '../stories/story-menu.component';
import { useLocation } from '@reach/router';
import { Constants } from '../../constants/constants';

const StandardMenu = () => {
    const mobile = false;
    const location = useLocation();

    return (
        <ul className="flex flex-col p-4 mt-4 rounded-lg md:flex-row md:space-x-8 md:mt-0 md:border-0 md:bg-white">
            <li>
                <StoryMenuComponent mobile={mobile}></StoryMenuComponent>
            </li>
            <li>
                <Link to="/blog" activeClassName="text-zinc-900 font-medium" className=" block py-2 pr-4 pl-3 md:p-0">
                    <span className={location.pathname.includes(Constants.BLOG_PATH) ? 'font-medium text black' : ''}>
                        blog
                    </span>
                </Link>
            </li>
            <li>
                <Link to="/about" activeClassName="text-zinc-900 font-medium" className="block py-2 pr-4 pl-3 md:p-0">
                    à propos
                </Link>
            </li>
            <li>
                <Link to="/contact" activeClassName="text-zinc-900 font-medium" className="block py-2 pr-4 pl-3 md:p-0">
                    contact
                </Link>
            </li>
        </ul>
    );
};

export default StandardMenu;
