import * as React from 'react';
import { Link } from 'gatsby';
import MobileMenuComponent from './mobile-menu.component';
import StandardMenuComponent from './standard-menu.component';

type State = { isMobileMenuActive: boolean };

class Header extends React.Component<{}, State> {
    constructor(props: {} | Readonly<{}>) {
        super(props);
        this.state = { isMobileMenuActive: false };
        this.handleMobileMenu = this.handleMobileMenu.bind(this);
    }

    handleMobileMenu() {
        this.setState({ isMobileMenuActive: !this.state.isMobileMenuActive });
    }

    render() {
        return (
            <header className="pt-10 mb-10 sticky top-0 z-50 bg-white">
                <nav className="border-gray-200 sm:px-4 rounded">
                    <div className="container md:flex md:flex-wrap justify-between items-center mx-auto">
                        <div className="cursor-pointer text-3xl text-center font-['Barcode'] md:text-start bg-gradient-to-r text-transparent bg-clip-text from-gray-900 to-zinc-50 block py-2 px-4 md:p-0">
                            <Link to="/" className="hidden w-full md:block md:w-auto">
                                maxime boulanghien
                            </Link>
                            <div className="md:hidden" onClick={this.handleMobileMenu}>
                                maxime boulanghien
                            </div>
                        </div>

                        <div className="hidden w-full md:block md:w-auto">
                            <StandardMenuComponent></StandardMenuComponent>
                        </div>
                    </div>
                    <div className="md:hidden" id="mobile-menu" hidden={!this.state.isMobileMenuActive}>
                        <MobileMenuComponent></MobileMenuComponent>
                    </div>
                </nav>
            </header>
        );
    }
}

export default Header;
