import { Link } from 'gatsby';
import * as React from 'react';
import { SiGitlab, SiInstagram, SiMatrix } from 'react-icons/si';

const Footer = () => {
    const now = new Date();
    return (
        <footer className="container py-10 md:py-8">
            <hr className="my-6 border-gray-100 sm:mx-auto lg:my-4"></hr>

            <div className="columns-1 md:columns-3">
                <div className="flex place-content-center md:place-content-start">
                    <a
                        href="https://www.instagram.com/maxjme.bn/"
                        className="flex items-center mb-4 mr-2"
                        target="blank"
                    >
                        <SiInstagram />
                    </a>
                    <a href="https://gitlab.com/maximebn/" className="flex items-center mb-4 mr-2" target="blank">
                        <SiGitlab />
                    </a>
                    <a
                        href="https://matrix.to/#/@maximebn:matrix.org"
                        className="flex items-center mb-4 mr-2"
                        target="blank"
                    >
                        <SiMatrix />
                    </a>
                </div>
                <div className="  mx-auto">
                    <div className="text-xs text-gray-500 flex place-content-center">
                        <a>© {now.getFullYear()} - Tous droits réservés.</a>
                    </div>
                </div>

                <div>
                    <ul className="flex flex-nowrap md:justify-end mb-6 text-sm place-content-center md:place-content-start">
                        <li>
                            <Link
                                to="/legal/carbon"
                                className="text-xs text-gray-500 hover:text-gray-900 block mr-4 pl-3 md:p-0 text-center md:text-start"
                                activeClassName="text-black font-medium"
                            >
                                impact carbone
                            </Link>
                        </li>
                        <li>
                            <Link
                                to="/legal/privacy"
                                className="text-xs text-gray-500 hover:text-gray-900 block mr-4 pl-3 md:p-0 text-center md:text-start"
                                activeClassName="text-black font-medium"
                            >
                                données
                            </Link>
                        </li>
                        <li>
                            <Link
                                to="/legal"
                                className="text-xs text-gray-500 hover:text-gray-900 block pl-3 md:p-0 text-center md:text-start"
                                activeClassName="text-black font-medium"
                            >
                                mentions légales
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        </footer>
    );
};
export default Footer;
