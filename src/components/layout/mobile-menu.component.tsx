import * as React from 'react';
import { Link } from 'gatsby';
import StoryMenuComponent from '../stories/story-menu.component';
import { useLocation } from '@reach/router';
import { Constants } from '../../constants/constants';

const MobileMenu = () => {
    const mobile = true;
    const location = useLocation();

    return (
        <ul className="flex flex-col p-4 mt-4 border border-gray-100 rounded-lg bg-gray-50 mx-5 text-gray-700">
            <li>
                <div
                    className={
                        location.pathname.includes(Constants.STORIES_PATH)
                            ? 'block py-2 pl-3 pr-4 text-white bg-sky-800 rounded md:bg-transparent'
                            : 'block py-2 pl-3 pr-4'
                    }
                >
                    <StoryMenuComponent mobile={mobile}></StoryMenuComponent>
                </div>
            </li>
            <li>
                <Link
                    to="/blog"
                    activeClassName="text-white bg-sky-800 rounded md:bg-transparent font-medium"
                    className="block py-2 pl-3 pr-4"
                >
                    <span className={location.pathname.includes(Constants.BLOG_PATH) ? 'font-medium text black' : ''}>
                        blog
                    </span>
                </Link>
            </li>
            <li>
                <Link
                    to="/about"
                    activeClassName="text-white bg-sky-800 rounded md:bg-transparent font-medium"
                    className="block py-2 pl-3 pr-4"
                >
                    à propos
                </Link>
            </li>
            <li>
                <Link
                    to="/contact"
                    activeClassName="text-white bg-sky-800 rounded md:bg-transparent font-medium"
                    className="block py-2 pl-3 pr-4"
                >
                    contact
                </Link>
            </li>
        </ul>
    );
};

export default MobileMenu;
