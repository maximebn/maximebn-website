import * as React from 'react';
import Footer from './footer.component';
import Header from './header.component';

type LayoutProps = { children: React.ReactNode };

const Layout = ({ children }: LayoutProps) => {
    return (
        <div>
            <div className="flex flex-col h-screen justify-between font-normal">
                <Header></Header>
                <div className="mb-auto">
                    <div>
                        <div className="content container max-w-screen-xl">{children}</div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        </div>
    );
};
export default Layout;
