import { graphql, useStaticQuery } from 'gatsby';
import * as React from 'react';
import BlogCard from './blog-card.component';

const BlogList = () => {
    // Données = le contenu du dossier /articles sous content
    const data: Queries.ArticlesQuery = useStaticQuery(graphql`
        query Articles {
            allMarkdownRemark(filter: { fileAbsolutePath: { regex: "/(articles)/" } }) {
                nodes {
                    timeToRead
                    frontmatter {
                        title
                        category
                        shortDescription
                        date
                        slug
                    }
                    id
                }
            }
        }
    `);

    // Template data
    const articles = data.allMarkdownRemark.nodes;

    return (
        <section className="px-4 lg:px-6">
            <div className="">
                {articles.map((article) => (
                    <div key={article.id} className="mb-3">
                        <BlogCard article={article}></BlogCard>
                    </div>
                ))}
            </div>
        </section>
    );
};

export default BlogList;
