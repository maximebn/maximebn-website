import * as React from 'react';

const BlogListBanner = () => {
    return (
        <div className="mx-auto max-w-screen-sm text-center lg:mb-16 mb-8 px-4">
            <h2 className="text-primary text-lg font-bold mb-2 text-center md:text-start">Blog</h2>
            <h2 className=" text-sky-800 text-3xl font-extrabold md:text-4xl text-center md:text-start">
                {' '}
                Mes aventures, mes projets, ici et ailleurs
            </h2>
            <h3 className="text-primary text-sm mb-6 md:mb-16 text-center md:text-start">
                Parcourez ces quelques articles écrits en toute modestie ⛰️
            </h3>
            <div>
                <img className="w-full w-10/12 mt-16 mx-auto" src="/vectors/undraw_japan_ubgk.svg" alt="blog-me" />
            </div>
        </div>
    );
};

export default BlogListBanner;
