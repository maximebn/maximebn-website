import { Link } from 'gatsby';
import * as React from 'react';
import { FiArrowRight } from 'react-icons/fi';
import { Constants } from '../../constants/constants';

const BlogCard = ({ article }: any) => {
    return (
        <article className="p-6 bg-white rounded-lg border border-gray-200 shadow-md">
            <div className="flex flex-row mb-5 justify-between text-gray-500">
                <div>
                    <span className="bg-primary-100 text-primary-800 text-xs font-medium px-2.5 py-0.5 rounded mr-2">
                        {article.frontmatter.category}
                    </span>
                    <span className="bg-yellow-100 text-yellow-800 text-xs font-medium px-2.5 py-0.5 rounded">
                        {article.frontmatter.date}
                    </span>
                </div>
                <div>
                    <span className="flex-auto text-sm justify-self-end"> lecture : {article.timeToRead} min</span>
                </div>
            </div>
            <h2 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">{article.frontmatter.title}</h2>
            <p className="mb-5 font-light text-gray-500">{article.frontmatter.shortDescription}</p>
            <div className="flex justify-between items-center">
                <Link
                    to={Constants.BLOG_PATH.concat(article.frontmatter.slug)}
                    key={article.id}
                    activeClassName="text-zinc-900 font-medium"
                >
                    <div className="inline-flex items-center font-medium text-primary-600 hover:underline">
                        Lire la suite
                        <FiArrowRight className="ml-2" />
                    </div>
                </Link>
            </div>
        </article>
    );
};

export default BlogCard;
