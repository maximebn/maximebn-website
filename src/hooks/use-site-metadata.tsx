import { useStaticQuery, graphql } from "gatsby";

export const useSiteMetadata = () => {
    const data: Queries.SiteMetaDataQuery = useStaticQuery(graphql`
        query SiteMetaData {
            site {
                siteMetadata {
                    title
                    description
                    siteUrl
                }
            }
        }
    `);
    return data.site?.siteMetadata;
};
