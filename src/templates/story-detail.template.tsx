import * as React from 'react';
import Layout from '../components/layout/layout.component';
import { graphql, PageProps } from 'gatsby';
import { SEO } from '../components/seo.component';
import Lightbox from 'yet-another-react-lightbox';
import 'yet-another-react-lightbox/styles.css';

const StoryDetailTemplate = ({ data }: PageProps<Queries.StoryDetailsQuery>) => {
    const { html }: any = data.stories;
    const frontmatter = data.stories?.frontmatter;
    const images = data.images.edges;
    // Couleurs déterminées dynamiquement à partir des données markdown
    const titleColorClassName = 'text-' + frontmatter?.titleColor;
    const iframeClassName = 'iframe w-full h-64 rounded-lg shadow-xl shadow-' + frontmatter?.titleColor + '/50'!;

    const [index, setIndex] = React.useState(-1);

    function closeLightBox(): void {
        setIndex(-1);
    }

    return (
        <Layout>
            <div className="px-5 md:px-0">
                <div className="mb-16">
                    <div className="mb-4 text-sm opacity-50">
                        {frontmatter?.place}, {frontmatter?.year}
                    </div>
                    <h2 className="mb-4 text-4xl font-extrabold">
                        <div className={titleColorClassName}> {frontmatter?.title}</div>
                    </h2>
                    <p className="mb-6 text-2xl font-regular">{frontmatter?.description}</p>
                    <div className="md:columns-2">
                        <div className="relative rounded-lg shadow-xl shadow-cyan-500/50 mb-6">
                            <iframe className={iframeClassName} src={frontmatter?.mapLink!}></iframe>
                        </div>
                        <div className="text-justify w-full" dangerouslySetInnerHTML={{ __html: html }}></div>
                    </div>
                </div>

                <div className="w-full lg:columns-4 sm:columns-2 gap-3">
                    {images.map((image: any, index: number) => (
                        <figure className="mb-3" key={`${image.node.asset_id}-image`} onClick={() => setIndex(index)}>
                            <img
                                className="mb-3 hover:brightness-75 hover:cursor-pointer duration-1000"
                                src={image.node.secure_url!}
                                alt={image.node.context?.custom?.alt!}
                            ></img>
                        </figure>
                    ))}
                </div>
            </div>
            <Lightbox
                open={index >= 0}
                index={index}
                close={closeLightBox}
                styles={lightBoxStyles}
                slides={images.map((image: Image) => {
                    return {
                        src: image.node.secure_url!,
                        key: image.node.asset_id
                    };
                })}
            />
        </Layout>
    );
};
export default StoryDetailTemplate;

/**
 *
 */
export const lightBoxStyles: { [key: string]: React.CSSProperties } = {
    container: {
        backgroundColor: 'white'
    },
    button: {
        color: '#075985',
        filter: 'none'
    }
};

/**
 * Composant SEO : réutilisation des données du fichier MD
 * @param param0
 * @returns
 */
export const Head = ({ data }: PageProps<Queries.StoryDetailsQuery>) => (
    <SEO
        title="Maxime Boulanghien"
        description={data.stories?.frontmatter?.title!}
        ogImageUrl={data.images.edges[0].node.secure_url!}
    ></SEO>
);

/**
 * Données MD + Cloudinary via GraphQL
 */
export const query = graphql`
    query StoryDetails($slug: String, $cloudinaryFolder: String) {
        stories: markdownRemark(frontmatter: { slug: { eq: $slug } }) {
            html
            frontmatter {
                title
                description
                shortDescription
                place
                year
                date
                mapLink
                openStreetMapLink
                icon
                titleColor
            }
        }
        images: allCloudinaryMedia(filter: { folder: { eq: $cloudinaryFolder } }) {
            edges {
                node {
                    secure_url
                    asset_id
                    context {
                        custom {
                            alt
                        }
                    }
                }
            }
        }
    }
`;
