import * as React from 'react';
import Layout from '../components/layout/layout.component';
import { graphql, PageProps } from 'gatsby';
import { SEO } from '../components/seo.component';

const BlogDetailTemplate = ({ data }: PageProps<Queries.BlogDetailsQuery>) => {
    const { html }: any = data.articles;
    const frontmatter = data.articles?.frontmatter;
    const images = data.images.edges;
    // Couleurs déterminées dynamiquement à partir des données markdown
    return (
        <Layout>
            <div className="px-5 md:px-0">
                <div className="mb-16">
                    <div className="mb-4">
                        <span className="bg-primary-100 text-primary-800 text-xs font-medium px-2.5 py-0.5 rounded mr-2 mb-8">
                            {frontmatter?.category}
                        </span>
                        <span className="bg-amber-100 text-xs font-medium px-2.5 py-0.5 rounded mr-2 mb-8">
                            écrit le {frontmatter?.date}
                        </span>
                    </div>

                    <h2 className="mb-4 text-4xl font-extrabold text-sky-800">{frontmatter?.title}</h2>
                    <p className="mb-6 text-2xl font-regular">{frontmatter?.shortDescription}</p>
                    <div className="w-full md:columns-2">
                        {images.map((image, index) => (
                            <figure className="mb-8" key={`${image.node.asset_id}-image`}>
                                <img
                                    className="mb-8"
                                    src={image.node.secure_url!}
                                    alt={image.node.context?.custom?.alt!}
                                ></img>
                            </figure>
                        ))}
                    </div>
                    <div className="md:columns-2">
                        <div className="text-justify w-full" dangerouslySetInnerHTML={{ __html: html }}></div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};
export default BlogDetailTemplate;

/**
 * Composant SEO : réutilisation des données du fichier MD
 * @param param0
 * @returns
 */
export const Head = ({ data }: PageProps<Queries.BlogDetailsQuery>) => (
    <SEO
        title="Maxime Boulanghien"
        description={data.articles?.frontmatter?.title!}
        ogImageUrl={data.images.edges[0].node.secure_url!}
    ></SEO>
);

/**
 * Données MD + Cloudinary via GraphQL
 */
export const query = graphql`
    query BlogDetails($slug: String, $cloudinaryFolder: String) {
        articles: markdownRemark(frontmatter: { slug: { eq: $slug } }) {
            html
            timeToRead
            frontmatter {
                title
                category
                shortDescription
                date
            }
        }
        images: allCloudinaryMedia(filter: { folder: { eq: $cloudinaryFolder } }) {
            edges {
                node {
                    secure_url
                    asset_id
                    context {
                        custom {
                            alt
                        }
                    }
                }
            }
        }
    }
`;
