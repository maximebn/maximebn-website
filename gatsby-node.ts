import type { GatsbyNode } from 'gatsby';
import { resolve } from 'path';
import { Constants } from './src/constants/constants';

export const createPages: GatsbyNode['createPages'] = async ({ actions, graphql, reporter }) => {
    const { createPage } = actions;

    // Fichiers md du dossier /stories
    const stories: { errors?: any; data?: Queries.StoriesPagesInformationQuery } = await graphql(`
        query StoriesPagesInformation {
            allMarkdownRemark(filter: { fileAbsolutePath: { regex: "/(stories)/" } }) {
                nodes {
                    frontmatter {
                        slug
                        cloudinaryFolder
                    }
                }
            }
        }
    `);

    // Fichiers md du dossier /articles
    const articles: { errors?: any; data?: Queries.ArticlesPagesInformationQuery } = await graphql(`
        query ArticlesPagesInformation {
            allMarkdownRemark(filter: { fileAbsolutePath: { regex: "/(articles)/" } }) {
                nodes {
                    frontmatter {
                        slug
                        cloudinaryFolder
                    }
                }
            }
        }
    `);

    // Handle errors
    if (articles.errors || stories.errors) {
        reporter.panicOnBuild(`Erreur pendant l'exécution d'une requête GraphQL`);
        return;
    }

    // Création des pages pour les stories
    stories.data?.allMarkdownRemark.nodes.forEach((node) => {
        const slug = node.frontmatter?.slug;
        const cloudinaryFolder = node.frontmatter?.cloudinaryFolder;
        createPage({
            path: Constants.STORIES_PATH + slug,
            component: resolve(`./src/templates/story-detail.template.tsx`),
            context: { slug, cloudinaryFolder }
        });
    });

    // Création des pages pour les articles
    articles.data?.allMarkdownRemark.nodes.forEach((node) => {
        const slug = node.frontmatter?.slug;
        const cloudinaryFolder = node.frontmatter?.cloudinaryFolder;

        createPage({
            path: Constants.BLOG_PATH + slug,
            component: resolve(`./src/templates/blog-detail.template.tsx`),
            context: { slug, cloudinaryFolder }
        });
    });
};
