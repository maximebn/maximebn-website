/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        './src/pages/*.{js,jsx,ts,tsx}',
        './src/pages/**/*.{js,jsx,ts,tsx}',
        './src/components/*.{js,jsx,ts,tsx}',
        './src/components/**/*.{js,jsx,ts,tsx}',
        './src/templates/*.{js,jsx,ts,tsx}'
    ],
    safelist: [
        {
            pattern: /text-(amber|cyan|sky|emerald|blue)-(500|600|700|800)/
        },
        {
            pattern: /shadow-(amber|cyan|sky|emerald|blue)-(500|600|700|800)/
        }
    ],
    theme: {
        extend: {
            container: {
                center: true
            },
            fontFamily: {
                body: ['Colfax', 'Noto Color Emoji'],
                sans: ['Colfax', 'Noto Color Emoji']
            },
            colors: {
                'default-color-theme': '0E3B46',
                'custom-deep-blue': '0E3B46',
                primary: {
                    50: '#f0f9ff',
                    100: '#e0f2fe',
                    200: '#bae6fd',
                    300: '#7dd3fc',
                    400: '#38bdf8',
                    500: '#0ea5e9',
                    600: '#0284c7',
                    700: '#0369a1',
                    800: '#075985',
                    900: '#0c4a6e'
                }
            }
        }
    },
    plugins: [require('@tailwindcss/typography')]
};
