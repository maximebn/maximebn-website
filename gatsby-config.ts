import { readFileSync, writeFileSync } from 'fs';
import type { GatsbyConfig } from 'gatsby';

require('dotenv').config({
    path: `env/.env.${process.env.NODE_ENV}`
});

const {
    NODE_ENV,
    URL: NETLIFY_SITE_URL = 'https://maximebn.com',
    DEPLOY_PRIME_URL: NETLIFY_DEPLOY_URL = NETLIFY_SITE_URL,
    CONTEXT: NETLIFY_ENV = NODE_ENV
} = process.env;
const isNetlifyProduction = NETLIFY_ENV === 'production';
const siteUrl = isNetlifyProduction ? NETLIFY_SITE_URL : NETLIFY_DEPLOY_URL;

const config: GatsbyConfig = {
    siteMetadata: {
        title: `Maxime Boulanghien - Photographie`,
        description: `Photographie documentaire & de voyage`,
        siteUrl
    },
    // More easily incorporate content into your pages through automatic TypeScript type generation and better GraphQL IntelliSense.
    // If you use VSCode you can also use the GraphQL plugin
    // Learn more at: https://gatsby.dev/graphql-typegen
    graphqlTypegen: true,
    plugins: [
        'gatsby-plugin-webpack-bundle-analyser-v2',
        'gatsby-plugin-postcss',
        'gatsby-plugin-image',
        'gatsby-plugin-mdx',
        'gatsby-transformer-remark',
        'gatsby-plugin-sharp',
        'gatsby-transformer-sharp',
        {
            resolve: `gatsby-plugin-sitemap`,
            options: {
                query: `{
                    site {
                        siteMetadata {
                            siteUrl
                        }
                    }
                    allSitePage {
                        nodes {
                            path
                            pageContext
                        }
                    }
                    allMarkdownRemark {
                        nodes {
                            frontmatter {
                                date
                                slug
                            }
                        }
                    }
                }`,
                resolveSiteUrl: () => siteUrl,
                resolvePages: ({
                    allSitePage: { nodes: allPages },
                    allMarkdownRemark: { nodes: allMarkdown }
                }: any) => {
                    const pathToDateMap: Record<string, any> = {};
                    allMarkdown.map((article: any) => {
                        pathToDateMap[article.frontmatter.slug] = { date: article.frontmatter.date };
                    });
                    const pages = allPages.map((page: { path: string; pageContext: { slug: string } }) => {
                        return { ...page, ...pathToDateMap[page.pageContext.slug] };
                    });
                    return pages;
                },
                serialize: ({ path, date }: { path: string; date: string }) => {
                    let entry = {
                        url: path,
                        changefreq: 'monthly',
                        priority: 0.5,
                        lastmod: ''
                    };

                    if (date) {
                        entry.priority = 0.7;
                        entry.lastmod = date;
                    }
                    return entry;
                }
            }
        },
        {
            resolve: 'gatsby-plugin-robots-txt',
            options: {
                resolveEnv: () => NETLIFY_ENV,
                env: {
                    production: {
                        host: siteUrl,
                        sitemap: siteUrl + '/sitemap-index.xml',
                        policy: [{ userAgent: 'Googlebot-Image', disallow: ['/'] }]
                    },
                    'branch-deploy': {
                        policy: [{ userAgent: '*', disallow: ['/'] }],
                        sitemap: null,
                        host: null
                    },
                    'deploy-preview': {
                        policy: [{ userAgent: '*', disallow: ['/'] }],
                        sitemap: null,
                        host: null
                    }
                }
            }
        },
        {
            resolve: 'gatsby-plugin-manifest',
            options: {
                icon: 'src/images/pinata.png'
            }
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `content`,
                path: `${__dirname}/src/content/`
            }
        },
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                name: 'images',
                path: './src/images/'
            },
            __key: 'images'
        },
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                name: 'pages',
                path: './src/pages/'
            },
            __key: 'pages'
        },
        {
            resolve: `gatsby-source-cloudinary`,
            options: {
                cloudName: process.env.CLOUDINARY_CLOUD_NAME,
                apiKey: process.env.CLOUDINARY_API_KEY,
                apiSecret: process.env.CLOUDINARY_API_SECRET,
                resourceType: `image`,
                maxResults: 100,
                tags: true,
                context: true
            }
        },
        {
            resolve: `gatsby-transformer-cloudinary`,
            options: {
                transformTypes: ['CloudinaryAsset'],
                defaultTransformations: ['c_fill', 'g_auto', 'q_auto', 'f_auto']
            }
        },
        // https://www.gatsbyjs.com/plugins/gatsby-plugin-csp/
        {
            resolve: `gatsby-plugin-csp`,
            options: {
                mergeScriptHashes: false,
                mergeStyleHashes: false,
                directives: {
                    'default-src': `'self' www.openstreetmap.org www.google.com app.netlify.com`,
                    'script-src': `'self' 'unsafe-inline' data: www.google.com www.gstatic.com netlify-cdp-loader.netlify.app`,
                    'style-src': `'self' 'unsafe-inline' fonts.googleapis.com fonts.gstatic.com`,
                    'img-src': `'self' data: res.cloudinary.com`,
                    'font-src': `'self' data: fonts.gstatic.com`
                }
            }
        },
        // https://gabrielkoo.com/blog/gatsby-netlify-csp-headers/
        {
            resolve: `gatsby-plugin-netlify`,
            options: {
                transformHeaders: (headers: string[], path: string) => {
                    if (path?.endsWith('/')) {
                        const filePath = `./public${path}index.html`;
                        const rawHtml = readFileSync(filePath).toString();
                        const csp = /<meta http-equiv="Content-Security-Policy" content="(.*?)"\/>/
                            .exec(rawHtml)![1]
                            .replace(/&#x27;/g, `'`);
                        headers.push(`Content-Security-Policy: ${csp}`);
                        writeFileSync(
                            filePath,
                            rawHtml.replace(/<meta http-equiv="Content-Security-Policy" content=".*?"\/>/g, '')
                        );
                    }
                    return headers;
                },
                mergeSecurityHeaders: true,
                mergeCachingHeaders: true,
                generateMatchPathRewrites: true
            }
        }
    ]
};

export default config;
