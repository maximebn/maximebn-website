![Netlify](https://img.shields.io/netlify/24dda545-d671-47e8-aa95-7efbcc26a2d5?logo=netlify&style=for-the-badge)
![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/maximebn/maximebn-website?branch=main&logo=gitlab&style=for-the-badge)
<img src="https://img.shields.io/static/v1?label=architecture&message=Jamstack&color=e7017a&style=for-the-badge&logo=jamstack&logoColor=e7017a&link=https://jamstack.org/">
![npm](https://img.shields.io/npm/v/gatsby?color=blueviolet&label=gatsby&logo=gatsby&style=for-the-badge)
![npm](https://img.shields.io/npm/v/react?color=blue&label=react&logo=react&style=for-the-badge)
![npm type definitions](https://img.shields.io/npm/types/gatsby?logo=typescript&style=for-the-badge)
![GitLab last commit](https://img.shields.io/gitlab/last-commit/41201746?style=for-the-badge)

<h1 style="text-align: center; margin-top: 1rem">Un simple site web de photographie, construit avec <a href="https://www.gatsbyjs.com/">Gatsby</a>.</h1>

## Prérequis 👾

Mises à jour : npx npm-check-updates -u + npm install

## Démarrage 🚀

devContainer

1.  **Create a Gatsby site.**

    Use the Gatsby CLI to create a new site, specifying the minimal TypeScript starter.

    ```shell
    # create a new Gatsby site using the minimal TypeScript starter
    npm init gatsby
    ```

2.  **Start developing.**

    Navigate into your new site’s directory and start it up.

    ```shell
    cd my-gatsby-site/
    npm run develop
    ```

3.  **Open the code and start customizing!**

    Your site is now running at http://localhost:8000!

    Edit `src/pages/index.tsx` to see your site update in real-time!

4.  **Learn more**

    -   [Documentation](https://www.gatsbyjs.com/docs/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter-ts)

    -   [Tutorials](https://www.gatsbyjs.com/tutorial/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter-ts)

    -   [Guides](https://www.gatsbyjs.com/tutorial/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter-ts)

    -   [API Reference](https://www.gatsbyjs.com/docs/api-reference/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter-ts)

    -   [Plugin Library](https://www.gatsbyjs.com/plugins?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter-ts)

    -   [Cheat Sheet](https://www.gatsbyjs.com/docs/cheat-sheet/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter-ts)

## Jamstack

Ce projet consiste en une réécriture du projet <a href="https://gitlab.com/maximebn/maximebn-photography">maximebn-photography</a> en une version reposant sur l'<a href="https://jamstack.org/">architecture Jamstack</a> : JavaScript, APIs, et markup HTML. Contrairement à sa précente version écrite en Python avec le <a href="https://www.djangoproject.com/">framework Django</a>, cette version Jamstack fonctionne sans base de données, ni serveur. Les avantages sont :

-   La performance : les temps de chargement sont beaucoup plus rapides (ni base ni serveur), ce qui pour la taille du projet paraît plus raisonnable. Cet aspect est aussi motivé par une réduction de l'empreinte carbone totale du site,
-   La facilité de déploiement : comme il s'agit d'un site statique, son déploiement est facile et peut prendre diverses formes, ce qui, encore une fois, pour la taille du projet et son objet parait plus raisonnable, d'autant plus que son coût sera moindre également,
-   La sécurité : moins de dépendances = moins de vulnérabilités.

## Stack technique 💤

### Gatsby

<a href="https://www.gatsbyjs.com/why-gatsby">Gatsby.js</a> est un générateur de sites statiques. C'est un framework open-source qui repose sur la librairie <a href="https://fr.reactjs.org/">React</a>.

#### Avec typescript

Gatsby est aujourd'hui <a href="https://www.gatsbyjs.com/docs/how-to/custom-configuration/typescript/">nativement compatible</a> avec l'utilisation de <a href="https://www.typescriptlang.org/">TypeScript</a>. Un peu de configuration est toutefois nécessaire :

-   Configuration de Gatsby : les fichiers `gatsby-node` et `gatsby-config` peuvent être écrits en Typescript : consulter la <a href="https://www.gatsbyjs.com/docs/how-to/custom-configuration/typescript/">documentation</a> pour les références complètes
-   <a href="https://www.gatsbyjs.com/docs/how-to/local-development/graphql-typegen/">TypeGen</a> : cette fonctionnalité permet de générer automatiquement les types des résultats des requêtes GraphQL. Sa configuration se résume à une ligne dans le fichier `gatsby-node.ts` : ` graphqlTypegen: true`. Préférer la lecture de la documentation.

#### Typography

plugin typography pour le contenu rendu MD. Donner exemples.

#### Markdown

Comment modifier un fichier, quoi faire pour ajouter une story. etc

#### Variables d'environnement

Les variables d'environnement se situent dans deux fichiers pour deux environnements différents :

-   `.env.development` pour l'environnement de développement,
-   `.env.production` pour l'environnement de production sur lequel est déployé le site.

Lire cette documentation pour tous les détails : <a href="https://www.gatsbyjs.com/docs/how-to/local-development/environment-variables/">variables d'environnement avec Gatsby.js</a>.

### Tailwind

<a href="https://tailwindcss.com/">Tailwind</a> est un framework CSS récent. Pour son installation/configuration, on peut consulter les recommandations issues de leur <a herf="https://tailwindcss.com/docs/guides/gatsby">documentation officielle</a>. Il s'agit de :

-   Installer `tailwind` et le plugin Gatsby `gatsby-plugin-postcss`
-   Générer les fichiers de configuration `tailwind.config.js` et `postcss.config.js`
-   Configurer ces derniers.

Deux points d'attention :

-   l'utilisation de l'attribut `className` plutôt que de l'attribut `class` pour la définition des classes de style des éléments HTML.
-   La définition d'une [safelist](https://tailwindcss.com/docs/content-configuration) contenant les classes CSS à générer par Tailwind. En effet, certaines classes utilisées dans ce projet ne peuvent pas être scannées par Tailwind afin d'être déterminées dynamiquement, c'est le cas des classes servant à définir la couleur du titre des stories ; cette couleur étant définie dans un fichier `.md` par l'attribut `titleColor`. Ainsi, la configuration des sources Tailwind (`tailwind.config.js`) est composée de la section `content` qui contient les patterns de fichiers à scanner, et la section `safelist` qui contient un pattern de classes Tailwind qui ne pourront pas être scannées car contenues dans un fichier markdown. Sans cela, ces classes peuvent ne pas être incluses dans le build :

```
content: [
        './src/pages/*.{js,jsx,ts,tsx}',
        './src/pages/**/*.{js,jsx,ts,tsx}',
        './src/components/*.{js,jsx,ts,tsx}',
        './src/components/**/*.{js,jsx,ts,tsx}',
        './src/templates/*.{js,jsx,ts,tsx}'
    ],
safelist: [
        {
            pattern: /text-(amber|cyan|sky|emerald|blue)-(500|600|700|800)/
        }
    ]
```

Le reste est spécifique à Tailwind dont on peut consulter la <a href="https://tailwindcss.com/docs/installation">documentation</a>.

### Icônes

Les icônes peuvent être choisies parmi celles proposées par le projet <a href="https://react-icons.github.io/react-icons/">react-icons</a> qui référence toutes les icônes des bibliothèques d'icônes les plus populaires comme <a href="https://react-icons.github.io/react-icons/icons?name=fa">Font Awesome</a> ou <a href="https://react-icons.github.io/react-icons/icons?name=si">Simple Icons</a>.

Une exception est faite pour les icônes utilisée dans le menu `StoryMenuComponent.tsx`. En effet, les icônes doivent dans ce cas être dynamiques, lues depuis un fichier `markdown`. Pour y arriver, il a été choisi d'introduire une variable `icon` sur ce fichier en question, cette variable pointant vers une icône au format `.svg`.

Décrire : gatsby-browser, gatsby-node, graphqlconfig.

## Bonnes pratiques 💣

### Linting

#### ESLint

#### remarklint

La publication de nouveau contenu repose sur l'ajout de fichiers écrits au format `markdown` dans le dossier `src/content`. Pour assurer l'uniformité de leur écriture, s'assurer que celle-ci respect les bonnes pratiques, plusieurs outils sont utilisés :

-   <a>remark</a> qui est un écosystème de plugins permettant de mieux intégrer les fichiers `markdown` à des besoins programmatiques. Par exemple, remark est utilisée dans Gatsby pour <em>parser</em> les fichiers `markdown` et les restituer sous forme HTML, grâce à un plugin Gatsby populaire (<a href="https://www.gatsbyjs.com/plugins/gatsby-transformer-remark/">gatsby-transformer-remark</a>). Ce dernier n'est pas un plugin remark mais bien un plugin Gatsby qui utilise remark sous le capot.
-   <a href="https://github.com/remarkjs/remark-lint">remarklint</a> qui est un plugin remark permettant de faire des vérifications syntaxiques de fichiers `markdown` sur la base de règles spécifiées
-   <a href="https://github.com/remarkjs/remark-lint/tree/main/packages/remark-preset-lint-recommended">remark-preset-lint-recommended</a> qui est un ensemble de règles réfletant les bonnes pratiques.

Il est donc recommandé à chaque ajout/modification d'un fichier `markdown` de lancer la commande `npm run lint` pour lancer, entre autres, leur vérification.

### Commits

Deux outils sont utilisés pour contrôler et garantir l'uniformité, la cohérence des commits : <a href="https://commitlint.js.org/#/">commitlint</a> et <a href="http://commitizen.github.io/cz-cli/">commitizen</a>.

commitLint est un outil d'aide au respect de bonnes pratiques d'écriture de commits, lesquelles sont décrites sous forme de règles dans une <a href="https://www.conventionalcommits.org/en/v1.0.0/">convention de commits (la <em>Conventional Commit)</em></a>. A partir de ces règles, commitlint peut vérifier qu’un commit particulier respecte bien lesdites règles de la convention. Il est ici intégré à <a href="https://typicode.github.io/husky/#/">Husky</a>, dans un <a href="https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks">hook</a> de type pre-commit de manière à vérifier localement (et donc rapidement) que ces règles ont bien été appliquées au commit demandé. Aucun exotisme quant à sa configuration dans ce projet, on peut donc se référer à la <a href="https://github.com/conventional-changelog/commitlint">documentation officielle</a> décrivant son installation et son utilisation :

-   Sa configuration est héritée de `@commitlint/config-conventional` dans le fichier `commitlint.config.js`
-   Le hook de type <em>pre-commit</em> est décrit dans le fichier `commit-msg` dans le dossier `.husky` à la racine du projet.

Pour éviter que commitlint ne crie à chaque commit, on peut utiliser commitizen. Il s’agit d’un outil en ligne de commande qui, cette fois, au lieu de vérifier que le commit est bien écrit, nous aide à l'écrire correctement. Sa configuration se résume à :

-   Son installation : `npm install --save-dev @commitlint/cz-commitlint commitizen`
-   L'ajout d'un fichier `.commitlintrc.yml` si l'on souhaite surcharger la configuration par défaut si besoin
-   Sa configuration dans le fichier `package.json` de façon à l'associer à commitlint :

```shell
"config": {
		"commitizen": {
			"path": "@commitlint/cz-commitlint"
		}
	}
```

-   L'ajout d'un script dans le fichier `package.json` :

```shell
  "scripts": {
    "commit": "cz"
  }
```

Les commits peuvent désormais être créés depuis la commande `npm run commit`. Se référer à la <a href="https://github.com/commitizen-tools/commitizen">documentation officielle</a> pour de plus amples détails.

## Formulaire de contact 📡

### Traitement

Le traitement des demandes de contact à partir du formulaire est entièrement géré par Netlify (Netlify Forms). On peut consulter ces documentations pour sa mise en place :

-   [Documentation Netlify](https://docs.netlify.com/forms/setup/),
-   [Documentation Gatsby comprenant un exemple](https://www.gatsbyjs.com/docs/building-a-contact-form/),
-   [Précisions sur les notifications](https://docs.netlify.com/forms/notifications/).

Une page personnalisée de redirection a été mise en place : `contact/success.tsx`.

### Protection contre les spams

La [protection contre les spams](https://docs.netlify.com/forms/spam-filters/) est assurée par :

-   Un honeypot, géré par Netlify
-   Le service [reCaptcha de Google](https://developers.google.com/recaptcha/docs/display).

Le service reCaptcha proposé par défaut par Netlify Forms n'est pas compatible avec les libraries Javascript, et ne permet pas d'ailleurs pas d'utiliser la version invisible. Quelques ajustements sont nécessaires :

-   Installer la librairie `google-react-recaptcha` (https://www.npmjs.com/package/react-google-recaptcha),
-   Ajouter le composant `ReCAPTCHA` avec l'attribut `size="invisible` dans le formulaire,
-   Personnaliser la méthode de soumission du formulaire ` onSubmit={handleSubmit}`,
-   Cette dernière consiste à récupérer un token reCaptcha et l'ajouter au formulaire (`g-recaptcha-response`) avant de le soumettre. En effet, les serveurs Netlify vérifient que cet attibut est bien présent et valide dans les données du formulaire, si ce dernier contient bien l'attribut `data-netlify-recaptcha="true`.
-   Ne pas oublier de renseigner les variables d'environnement `SITE_RECAPTCHA_KEY` et `SITE_RECAPTCHA_SECRET`.

On peut consulter cet [article](https://blog.larsbehrenberg.com/how-to-create-a-contact-form-with-recaptcha-hosted-on-netlify) et [celui-ci](https://github.com/dozoisch/react-google-recaptcha#invisible-recaptcha) pour plus de détails.

## Gestion des images 🌪️

Les images et photos publiées sur ce site sont stockées sur [Cloudinary](https://cloudinary.com/) qui :

-   fournit un espace de stockage en ligne et centralisé de ces ressources,
-   fournit ces ressources par un CDN (réseau de distribution de contenu), de façon rapide et optimisée,
-   permet d'éviter de devoir intégrer ces ressources au code : ces usages sont donc clairement séparés.

L'utilisation de Cloudinary avec Gatsby repose sur le plugin `gatsby-source-cloudinary` dont on peut lire la documentation [ici](https://www.gatsbyjs.com/plugins/gatsby-source-cloudinary/). Quelques points d'attention sur sa configuration :

-   Les variables d'environnement `CLOUDINARY_CLOUD_NAME`, `CLOUDINARY_API_KEY` et `CLOUDINARY_API_SECRET` doivent être renseignées,
-   Le nombre maximum d'éléments remontées est fixé à 100, par l'option `maxResults`.

La recherche des données Cloudinary ne présente pas d'autres particularités, et repose sur le langage [GraphQL intégré à Gatsby](https://www.gatsbyjs.com/docs/graphql/). On peut consulter [cette page](https://github.com/cloudinary-devs/gatsby-source-cloudinary) pour un exemple.

## Intégration continue 🙈

L'intégration continue se résume à une étape d'analyse statique, qui repose fortement sur les <a href="https://docs.gitlab.com/ee/ci/testing/">différents outils proposés par Gitlab CI/CD</a>. Le déploiement étant réalisé sur Netlify qui propose son propre mode de déploiement (site statique), l'idée est simplement de contrôler, sommairement, la qualité des nouvelles versions de code du projet. La configuration du pipeline est décrite dans le fichier `gitlab-ci.yml`.

### Performance

La performance du site web déployée est mesurée grâce à <a href="">SiteSpeed</a> : il s'agit d'un ensemble d'outils open-source permettant de mesurer et monitorer diverses métriques liées à la performance d'un site web, comme le temps de chargement de tous les éléments visuels d'une page ou encore le temps d'affichage complet de l'image la plus lourde. A partir de ces métriques, un score global est attribué au site web, et permet, pour chaque nouveau déploiement, d'évaluer l'impact des modifications apportées à la nouvelle version de code.

L'utilisation de SiteSpeed dans le pipeline d'intégration continue est très simple grâce au travail de Gitlab qui l'intègre nativement grâce au <a href="https://gitlab.com/gitlab-org/gl-performance">développement d'un plugin</a>. Se référer à la <a href="https://docs.gitlab.com/ee/ci/testing/browser_performance_testing.html">documentation officielle</a> pour plus de détails.

### Sécurité

Une analyse statique de sécurité est réalisée de façon à vérifier qu'aucune vulnérabilité connue n'affecte le code déployé. Les résultats sont émis sous forme d'un fichier .json consultable en tant qu'artefact du job concerné. Se référer à la <a href="https://docs.gitlab.com/ee/user/application_security/sast/">documentation officielle</a> pour plus de détails.

### Qualité

La qualité de code est mesurée par <a href="https://docs.codeclimate.com/">Code Climate</a> par le biais d'une <a href="https://gitlab.com/gitlab-org/ci-cd/codequality/-/tree/master/codeclimate_defaults">configuration standard fournie par Gitlab</a>. Cette dernière peut être surchargée en modifiant le fichier `.codeclimate.yml` à la racine du projet. On peut consulter la <a href="https://docs.gitlab.com/ee/ci/testing/code_quality.html">documentation officielle</a> pour plus de détails sur son implémentation et son intégration dans Gitlab.

## Déploiement 🔥

Description déploiemetn continu Netlify : mais libre.

## SEO 🔎

### Composants SEO

https://www.gatsbyjs.com/docs/how-to/adding-common-features/adding-seo-component

### Génération du sitemap

La génération du sitemap repose sur l'utilisation du plugin [`gatsby-plugin-sitemap`](https://www.gatsbyjs.com/plugins/gatsby-plugin-sitemap/?=sitemap). Celui-ci permet de générer automatiquement le fichier correspondant, mais celui-ci est ici surchargé par une configuration custom dans le fichier `gatsby-config.ts`. La première étape consiste à écrire une requêtre GraphQL pour récupérer les données nécessaires à l'écriture du contenu du fichier `sitemap.xml`. Ici, on récupère le chemin associé à chaque page générée par Gatsby et la date associée à chaquef fichier Markdown :

```
                query: `{
                    site {
                        siteMetadata {
                            siteUrl
                        }
                    }
                    allSitePage {
                        nodes {
                            path
                            pageContext
                        }
                    }
                    allMarkdownRemark {
                        nodes {
                            frontmatter {
                                date
                                slug
                            }
                        }
                    }
                }`
```

Dans un second temps, le contenu des pages est surchargé, il s'agit d'y ajouter une date de mise à jour, laquelle est déterminée par l'attribut `date` associé à chaque fichier Markdown. Pour y parvenir, une map associe le slug de chaque article à cette date, laquelle est ensuite utilisée pour associer chaque date à la page correspondante :

```
                resolvePages: ({
                    allSitePage: { nodes: allPages },
                    allMarkdownRemark: { nodes: allMarkdown }
                }: any) => {
                    const pathToDateMap: Record<string, any> = {};
                    allMarkdown.map((article: any) => {
                        pathToDateMap[article.frontmatter.slug] = { date: article.frontmatter.date };
                    });
                    const pages = allPages.map((page: { path: string; pageContext: { slug: string } }) => {
                        return { ...page, ...pathToDateMap[page.pageContext.slug] };
                    });
                    return pages;
                },
```

Enfin, le fichier `sitemap.xml` est généré :

```
                serialize: ({ path, date }: { path: string; date: string }) => {
                    let entry = {
                        url: path,
                        changefreq: 'monthly',
                        priority: 0.5,
                        lastmod: ''
                    };

                    if (date) {
                        entry.priority = 0.7;
                        entry.lastmod = date;
                    }
                    return entry;
                }
```

Le principe est surtout d'ajouter une date de mise à jour pour chaque page où c'est nécessaire afin de coller à la [documentation Google](https://developers.google.com/search/docs/crawling-indexing/sitemaps/build-sitemap?hl=en&visit_id=638087946478401302-379854197&rd=1). Si l'on souhaite tester la génération de ce fichier `sitemap.xml` en mode développement :

-   Dans le fichier `gatsby-config.ts`, modifier toutes les URLs (siteUrl etc) du site par `http://localhost:9000`,
-   Lancer la commande `gatsby build && gatsby serve` (mode production),
-   Se rendre à `http://localhost:9000/sitemap-index.xml` pour visualiser la liste des fichiers xml générés, et les consulter.

### Fichier `robots.txt`

Se référer à la documentation du plugin [`gatsby-plugin-robots-txt`](https://www.gatsbyjs.com/plugins/gatsby-plugin-robots-txt/?=robot) utilisé pour générer le fichier `robots.txt`, en particulier la configuration liée à la gestion des environnements de déploiement Netlify. L'unique particularité de la configuration réside dans la désactivation de l'indexation des images (désactivation du Googlebot-Image plus particulièrement):

```
policy: [{ userAgent: 'Googlebot-Image', disallow: ['/'] }]
```

## Empreinte carbone 🥬

Faire l'empreinte carbone du site : website carbon
[activeClassName](https://greenpixie.com/)
https://ecograder.com/

## Ressources 📚

En plus des différents liens mentionnés dans ce readme, on peut consulter ces ressources :

-   <a href="https://www.gatsbyjs.com/docs/">Documentation officielle de Gastby</a>
-   <a href="https://docs.netlify.com/">Documentation offcielle de Netlify</a>
-   L'<a href="https://jamstack.wtf/">architecture Jamstack décrite par Gatsby</a>
